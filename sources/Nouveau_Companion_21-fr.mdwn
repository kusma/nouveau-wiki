[[!table header="no" class="mointable" data="""
 [[Accueil|FrontPage-fr]]  |  [[TiNDC 2006|IrcChatLogs-fr]]  |  [[TiNDC 2007|IrcChatLogs-fr]]  |  [[Archives anciennes|IrcChatLogs-fr]]  |  [[Archives récentes|IrcChatLogs-fr]]  | DE/[[EN|Nouveau_Companion_21]]/[[ES|Nouveau_Companion_21-es]]/FR/RU/[[Team|Translation_Team]] 
"""]]


## Le compagnon irrégulier du développement de Nouveau (TiNDC)


## Édition du 10 juin 2007


### Introduction

Bonjour et bienvenue dans l'édition numéro 21 du TiNDC. 

Avant toute chose, je voudrais spécialement remercier tous les contributeurs et testeurs de ce projet. Non seulement les développeurs mais également vous, qui avez une place importante. Remerciements aussi aux membres des équipes de traduction qui vont jusqu'à traduire le TiNDC à temps pour la publication de la version anglaise. 

ABCLinux a réalisé une interview d'Andy Ritger, Directeur des logiciels Unix chez nVidia ; et l'une des questions portait sur Nouveau et ce que nVidia envisageait à notre propos : 

{{{ NVIDIA's stance is to neither help nor hinder Nouveau. We are committed 

* to supporting Linux through a) an open source 2d "nv" X driver which NVIDIA engineers actively maintain and improve, and b) our fully featured proprietary Linux driver which leverages common code with the other platforms that NVIDIA supports. }}} 
**NDT : Ce qui pourrait se traduire par :** 

{{{ La position de nVidia est de ne pas aider ni entraver Nouveau. Nous supportons Linux au travers de a) un pilote libre "nv" pour X que les développeurs nVidia maintiennent et améliorent, et b) notre pilote pleinement fonctionnel propriétaire nvidia qui bénéficie d'un code commun avec les autres plateformes supportées. }}} 

L'interview complète est disponible [[à cette adresse|http://www.abclinuxu.cz/clanky/rozhovory/andy-ritger-nvidia?page=1]]. 

Finalement, c'est peut-être le mieux que nous pouvons espérer. 

Du côté du conservatoire des Logiciels libres (SFC), et donc des 10 000 $ de dons, la réunion du conseil d'administration a eu lieu et les membres ont exprimé des réserves sur notre adhésion. un complément d'informations techniques a été demandées à Marcheu qui s'attache maintenant à le rédiger avant de l'envoyer. Avec un peu de chance, nous aurons une décision définitive bientôt. 

Finalement, nous avons découvert que nous avions de l'espace disque qui nous a été alloué lorsque nous avons migré vers les serveurs fd.o. Nous avons donc promptement déplacés les dumps depuis SourceForge vers [[fd.o|http://nouveau.freedesktop.org/tests/]]. Actuellement, nous sommes en attente d'un compte pour faire tourner le script d'upload automatique de kmeyer mais ça ne devrait plus prendre trop longtemps. 


### Statut actuel

Passons maintenant à la partie dont je parierais qu'elle vous intéresse le plus : la partie technique et ses progrès. 

Ahuillet continue son travail de collection des informations techniques sur Xv et comment l'implémenter ; il est ainsi l'auteur d'une page sur le sujet dans notre wiki ([[ici|ArthurHuillet]] ; voir également [[là|http://nouveau.freedesktop.org/wiki/XvMC]], d'un autre auteur pour XvMC). Il prévoit de commencer à travailler sur le code à partir du 20 juin environ et a promis de nous tenir informé de ses progrès dans les TiNDC qui seront publiés durant l'été. 

Nous sommes toujours en train de lutter pour faire fonctionner les 8600GT que les développeurs ont acquis. La plupart du travail est réalisé par Darktama, assisté de KoalaBR. 

La carte de KoalaBR fonctionne maintenant en 2D avec la branche nv50 de Darktama. Quelques omissions mineures y ont été corrigées (mais pas encore envoyé dans le dépôt public, pour différentes raisons exposées ci-dessous). 

Le passage au mode texte (depuis X) produit un certains nombre de choses intéressantes, mais le mode texte n'est pas une de celles là :). KoalaBR essaya de voler du code de NV utilisant l'int10 pour appeler le BIOS. Le vol étant immoral, Marcheu arrêta et réprimanda immédiatement KoalaBR (bon, pour être honnête, la vraie raison est plus technique, ce code n'était tout simplement pas portable vers d'autres architectures que linux x86 et x86_64). 

Airlied a fourni une version améliorée de vbetool qui aurait du permettre de réaliser les appels au BIOS via une couche d'émulation et de visualiser tous les accès aux registres de la carte. Malheureusement, l'émulation n'a pas fonctionné. Après quelques ruminations, airlied s'est rappelé l'existence d'une autre version de cet outil qui se contente d'imprimer via printf() les données nécessaires. Étant occupé à plein temps par des tests dans son travail, airlied a fourni le lien quelques jours plus tard. Par malchance, L'affichage des polices de la console apparait toujours corrompu et l'écran se retrouve en mode suspend (« no signal »). Un passage manuel du mode texte vers X et retour au mode texte permet de récupérer un affichage mais les polices apparaissent toujours corrompues et doivent être rechargées. 

Lors d'une discussion sur les options restantes, Marcheu et airlied se sont rappelés l'existence d'un patch pour x86emu (Xorg) réalisé par nVidia. Ce dernier expliquant qu'il était nécessaire au bon fonctionnement des cartes G8x. La prochaine étape est donc d'essayer cette version et de voir si les outils fonctionnent. Le but étant de documenter les accès aux registres afin de développer un petit outil qui irait écrire dans les registres et permettrait au changement de mode de fonctionner correctement. Et quand il fonctionnera pour la majorité des cartes G8x, nous intègrerons le changement de mode directement dans le pilote. 

Notez que même si ce travail peut être utile pour randr12, je suis seulement en train de parler, et d'essayer de faire fonctionner, du passage de X vers le mode texte. 

Darktama pense avoir réussi à déterminer un certains nombre de fonctions 3D telles que les adresses des shaders et la gestion de la mémoire virtuelle par les clients OpenGL. La gestion de la mémoire virtuelle serait très utile pour gérer les accès directs à la mémoire (DMA) au travers des bus PCI et PCI express ; sans surprise, il a également codé un prototype d'un backend TTM (Translation Table Maps) qui réalise ces accès DMA (ce backend n'est d'ailleurs pas spécifique des cartes G8x). Il semble que les cartes G8x adressent la mémoire sur 64bits tandis que les anciennes l'adressent sur 32bits. 

Finalement, jkolb a jeté un oeil sur le problème avec les NV3x et réussi à ce que sa carte « lance » glxgears. Malheureusement, le soucis évoqué dans le  [[TiNDC 15|Nouveau_Companion_15]] (matrice de projection non appliqué) est revenu à la charge, et il se décida à y regarder d'un peu plus près. Rapidement, il nota qu'aucune des fonctions d'initialisation des NV3x n'envoyait les données requises, et offrit d'essayer de corriger tout cela. Marcheu confirma que la matrice est actuellement gérée par les shaders et que ceux-ci sont uniquement disponible à partir des NV4x. Quand on utilise la version software, des bogues apparaissent et la matrice est soit appliquée deux fois ou pas du tout (comme dans ce cas). Cette fonction très innovante est sous la protection d'un brevet logiciel détenu par Marcheu qui a promis de regarder la semaine prochaine. 

Avec Marcheu de retour après la soumission de son manuscrit de thèse d'ici la semaine prochaine, nous devrions voir des progrès survenir sur d'autres architectures également. 


### Demande d'assistance

Comme toujours, les habituels suspects : renouveau et mmio dumps, tests divers et variés de Nouveau 

Les codeurs qui voudraient aider ne risquent pas non plus d'être rejetés :) 

[[<<< Édition précédente|Nouveau_Companion_20-fr]] | [[Édition suivante >>>|Nouveau_Companion_22-fr]] 
